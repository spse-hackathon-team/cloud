__version__ = "0.1.0"
import flask

def CreateApp():
    app = flask.Flask(__name__)
    from app.routes import route
    route(app)
    return app

app = CreateApp()